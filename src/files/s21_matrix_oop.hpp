#ifndef S21_MATRIX_OOP
#define S21_MATRIX_OOP

#include <algorithm>
#include <cmath>
#include <iostream>
#include <vector>

class S21Matrix {
 private:
  int rows_, cols_;
  double** matrix_;

 public:
  // constructor
  S21Matrix();
  S21Matrix(int rows, int cols);
  S21Matrix(const S21Matrix& other);
  S21Matrix(S21Matrix&& other) noexcept;

  // destructor
  ~S21Matrix();

  // operations
  bool EqMatrix(const S21Matrix& other) const;
  void SumMatrix(const S21Matrix& other);
  void SubMatrix(const S21Matrix& other);
  void MulNumber(const double num) noexcept;
  void MulMatrix(const S21Matrix& other);
  S21Matrix Transpose() noexcept;
  S21Matrix CalcComplements();
  double Determinant();
  S21Matrix InverseMatrix();

  // operators
  S21Matrix operator+(const S21Matrix& other) const;
  S21Matrix operator-(const S21Matrix& other) const;
  S21Matrix operator*(const S21Matrix& other) const;
  S21Matrix operator*(double other) const;
  friend S21Matrix operator*(double temp, const S21Matrix& other);
  bool operator==(const S21Matrix& other) const;

  S21Matrix& operator=(const S21Matrix& other);
  S21Matrix& operator-=(const S21Matrix& other);
  S21Matrix& operator+=(const S21Matrix& other);
  S21Matrix& operator*=(const S21Matrix& other);
  S21Matrix& operator*=(double other);
  double& operator()(int row, int col) const;

  // accessors
  int getRows() const;
  int getCols() const;
  double** getMatrix() const;
  double getElement(int i, int j) const;

  // mutators
  void setRows(int rows);
  void setCols(int cols);

  // other
  // void printMatrix(const S21Matrix& other);
  void setMatrix(double** values, int rows, int cols);

 private:
  void getMinor(S21Matrix other, S21Matrix& temp, int row, int col) noexcept;
  double getDet(S21Matrix& temp) noexcept;
};

#endif
